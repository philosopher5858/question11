<!DOCTYPE html>
<html lang="en">
<head>
    <style>
              .header{
    position:relative;
    display: block;
    width: 350px;
    height: 580px;
    border-radius: 10px;
    padding-top: 40px;
    padding-bottom: 50px;
    font-size: 17px;
    box-sizing: border-box;
    background:#000000;
    box-shadow: 4px 4px 8px #3a3d40, -4px -4px 8px rgb(76, 69, 69);
    letter-spacing: 2px;
    color: #ecf0f3;
    
  }
  
  body {
    margin: 0;
    width: 100vw;
    height: 100vh;
    background: #000000;
    display: flex;
    align-items: center;
    text-align: center;
    justify-content: center;
    place-items: center;
    overflow: hidden;
    font-family: 'Courier New', Courier, monospace;
  }
  
  .container {
    position: relative;
    width: 500px;
    height: 100px;
    border-radius: 20px;
    padding: 40px;
    color: #ecf0f3;
    /* box-sizing: border-box; */
    background:#000000;
    box-shadow: 4px 4px 8px #3c3e40, -4px -4px 8px rgb(58, 53, 53);
    letter-spacing: 4px;
    font-size: 25px;
    
  }
  
  .inputs {
    text-align: left;
    margin-top: 3px;
  }
  
  label, input, button {
    display:inline;
    width: 100%;
    padding: 0;
    border: none;
    outline: none;
    box-sizing: border-box;
  }
  .lable2{
    margin-bottom: 40px;
    padding-right: 100%;
    font-family: 'Courier New', Courier, monospace;
  }
  
  label {
    margin-bottom: 40px;
    padding-right: 100%;
    font-family: 'Courier New', Courier, monospace;
  }
  
  label:nth-of-type(2) {
    margin-top: 12px;
    padding-right: 0%;
  }
  
  input::placeholder {
    color: gray;
  }
  
  input {
    background: #f7f9fb;
    padding: 10px;
    padding-left: 20px;
    height: 50px;
    font-size: 14px;
    border-radius: 15px;
    box-shadow: inset 6px 6px 6px #62686e, inset -6px -6px 6px rgb(82, 85, 85);
  }
  
  button {
    color: black;
    margin-top: 20px;
    background: linear-gradient(white,black);
    height: 40px;
    width: 220px;
    border-radius: 7px;
    cursor: pointer;
    font-weight: 900;
    /* box-shadow: 2px 2px 6px #212426, -2px -2px 6px rgb(36, 33, 33); */
    transition: .1s;
  }
  
  button:hover {
    box-shadow: none;
    background: linear-gradient(black,white);
    color: black;
  }
  
  a {
    position: relative;
    font-size: 8px;
    bottom: 4px;
    right: 4px;
    text-decoration: none;
    color: black;
    border-radius: 10px;
    padding: 2px;
  }
    </style>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <div class="header">
    <p> <b>Question 11</b></p>  <br>	WAP to convert temperature from degree Celsius to Fahrenheit & vice-versa. Use the formula : F=9C/5 + 32   
    </div>
    <form>
    <div class="container">

    <?php
   
if($_SERVER['REQUEST_METHOD']=='POST'){

    $temp = $_POST['temp'];
   

    if(isset($_POST['con_typ'])){
      $type = $_POST['con_typ'];
      print "INPUT: ".$temp."<br><br>";
      if($type=="Celsius"){
        $celcius = ((9/5*($temp-32)));
        echo "Celsius: ".$celcius."<sup>0</sup>C<br>";
      }elseif($type=="Fahrenheit"){
        $fahrenheit = ((9*($temp)/5)+32)." F";
        echo "Fahrenheit: ".$fahrenheit."<br>";
      }
    

}
}

?>
<a href="Question11.html">
 <button type="button" value="GO BACK"> Go Back</button>
</a>
    </div>
</form>
    
</body>
</html>
